package com.example.r12.revoluttest.logical.data_sources.rates

import com.example.r12.revoluttest.entities.Rates

interface RatesDataSource {

    suspend fun getLastRequestedType(): String?

    suspend fun requestRates(base: String): Rates?

    suspend fun requestSaveRates(rates: Rates)

}