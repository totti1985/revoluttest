package com.example.r12.revoluttest

import androidx.multidex.MultiDexApplication
import com.example.r12.revoluttest.di.AppComponent
import com.example.r12.revoluttest.di.DaggerAppComponent
import com.example.r12.revoluttest.di.modules.ContextModule
import com.example.r12.revoluttest.di.modules.RetrofitModule

open class RevolutApplication : MultiDexApplication() {

    companion object {

        private lateinit var appComponent: AppComponent

        fun getAppComponent() = appComponent

    }

    override fun onCreate() {
        super.onCreate()
        appComponent = initAppComponent()
    }

    protected open fun initAppComponent(): AppComponent {
        return DaggerAppComponent.builder()
                .contextModule(ContextModule(this))
                .retrofitModule(RetrofitModule(BuildConfig.BASE_URL))
                .build()
    }

}