package com.example.r12.revoluttest.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.r12.revoluttest.db.dao.RatesDao
import com.example.r12.revoluttest.db.entities.RatesRoom

@Database(entities = [RatesRoom::class], version = 1, exportSchema = false)
abstract class RoomDatabase : RoomDatabase() {

    abstract fun getRatesDao(): RatesDao

}