package com.example.r12.revoluttest.db.converters

import androidx.room.TypeConverter
import java.util.*

class DateConverter {

    @TypeConverter
    fun toLong(date: Date): Long = date.time

    @TypeConverter
    fun fromLong(value: Long) : Date = Date(value)

}