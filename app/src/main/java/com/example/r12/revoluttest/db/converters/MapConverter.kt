package com.example.r12.revoluttest.db.converters

import androidx.room.TypeConverter

class MapConverter {

    companion object {

        private const val SEPARATOR = ";"
        private const val DIVIDER = "_"

    }

    @TypeConverter
    fun toString(map: Map<String, Double>): String {
        val builder = StringBuilder(map.size)
        map.forEach {
            val name = it.key
            val rate = it.value
            builder.append(name)
            builder.append(DIVIDER)
            builder.append(rate)
            builder.append(SEPARATOR)
        }
        return builder.toString()
    }

    @TypeConverter
    fun fromString(value: String) : Map<String, Double> {
        val map = mutableMapOf<String, Double>()
        val list = value.split(SEPARATOR)

        list.forEach {
            val name = it.substringBefore(DIVIDER)
            val value = it.substringAfter(DIVIDER).toDoubleOrNull()
            value?.let {
                map.put(name, value)
            }

        }
        return map
    }


}