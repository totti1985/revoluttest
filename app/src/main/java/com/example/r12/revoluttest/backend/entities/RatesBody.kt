package com.example.r12.revoluttest.backend.entities

import com.example.r12.revoluttest.entities.Rates
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RatesBody(
        @Expose
        @SerializedName("base")
        override val baseCurrency: String,
        @Expose
        @SerializedName("rates")
        override val dependantRatesMap: Map<String, Double>
) : Rates