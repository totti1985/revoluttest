package com.example.r12.revoluttest.tools

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.example.r12.revoluttest.extensions.firstToIntOrNull
import com.example.r12.revoluttest.extensions.secondToIntOrNull

class FormatTextWatcher(private val editText: EditText) : TextWatcher {

    companion object {

        private val DIGIT_REGEX = Regex("[^0-9.,]")
        private const val EMPTY_STRING = ""

    }

    override fun afterTextChanged(s: Editable?) {
        // nothing
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        // nothing
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        val original = s?.toString()
        val formattedValue = formatValue(original)

        if (original != formattedValue) {
            editText.removeTextChangedListener(this)
            editText.setText(formattedValue)
            editText.addTextChangedListener(this)
            editText.setSelection(formattedValue?.length ?: 0)
        }
    }

    private fun formatValue(value: String?): String? {
        return value?.replace(DIGIT_REGEX, EMPTY_STRING)
                .removeNonDigitsOnStart()
                .removeZeroOnStart()
    }

    private fun String?.removeNonDigitsOnStart(): String? {
        if (this.isNullOrEmpty()) return this
        if (!first().isDigit()) return substring(1).removeNonDigitsOnStart()
        return this
    }

    private fun String?.removeZeroOnStart(): String? {
        if (this.isNullOrEmpty() || length <= 1) return this

        if (firstToIntOrNull() == 0 && secondToIntOrNull() != null) {
            return substring(1).removeZeroOnStart()
        }
        return this
    }

}