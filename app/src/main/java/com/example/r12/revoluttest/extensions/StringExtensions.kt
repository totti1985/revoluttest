package com.example.r12.revoluttest.extensions

import java.text.DecimalFormat
import java.text.NumberFormat

private val DEFAULT_FORMATTER : NumberFormat = DecimalFormat("0.####")

fun Double?.formatToString(formatter: NumberFormat = DEFAULT_FORMATTER): String? {
    if (this == null) return null
    return formatter.format(this)
}

fun String?.firstToIntOrNull(): Int? = toIntOrNull(0)

fun String?.secondToIntOrNull(): Int? = toIntOrNull(1)

fun String?.toIntOrNull(position: Int): Int? {
    if (this.isNullOrEmpty()) return null
    if (position < 0 || position > this.length - 1) return null
    return substring(position, position + 1).toIntOrNull()
}