package com.example.r12.revoluttest.logical.data_sources.rates

import com.example.r12.revoluttest.db.dao.RatesDao
import com.example.r12.revoluttest.db.entities.RatesRoom
import com.example.r12.revoluttest.entities.Rates
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs

class RoomRatesDataSource(private val ratesDao: RatesDao) : RatesDataSource {

    companion object {

        private val CASH_TIME_IN_MILLIS = TimeUnit.DAYS.toMillis(1)

    }

    override suspend fun requestSaveRates(rates: Rates) {
        ratesDao.deleteAndInsert(RatesRoom(rates))
    }

    override suspend fun requestRates(base: String): Rates? {
        val rates = ratesDao.getRates(base)
        rates?.let {
            val now = Date()
            val savedTime = it.savedTime
            if (abs(now.time - savedTime.time) >= CASH_TIME_IN_MILLIS) {
                ratesDao.delete(it)
                return null
            }
        }
        return rates
    }

    override suspend fun getLastRequestedType() = ratesDao.getLastSavedRate()?.baseCurrency

}