package com.example.r12.revoluttest.di.modules

import android.content.Context
import androidx.room.Room
import com.example.r12.revoluttest.db.RoomDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomDatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): RoomDatabase {
        return Room.databaseBuilder(context, RoomDatabase::class.java, "database").build()
    }

}