package com.example.r12.revoluttest.ui.screens.main

import android.text.Editable
import android.text.TextWatcher
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.r12.revoluttest.R
import com.example.r12.revoluttest.entities.Currency
import com.example.r12.revoluttest.extensions.formatToString
import com.example.r12.revoluttest.tools.FormatTextWatcher
import com.example.r12.revoluttest.ui.screens.base.list.BaseListAdapter

class CurrencyAdapter(var callback: Callback? = null) : BaseListAdapter<Currency>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    inner class ViewHolder(
            parent: ViewGroup
    ) : BaseListAdapter.ViewHolder<Currency>(R.layout.item_currency, parent), TextWatcher {

        private val nameTextView: TextView = itemView.findViewById(R.id.textview_name)
        private val valueEditText: EditText = itemView.findViewById(R.id.edittext_value)

        init {
            valueEditText.addTextChangedListener(FormatTextWatcher(valueEditText))
            valueEditText.addTextChangedListener(this)
            valueEditText.setOnFocusChangeListener { _, hasFocus ->
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    callback?.onFocusChanged(list[position], hasFocus)
                }
            }
        }

        override fun bind(item: Currency) {
            nameTextView.text = item.name
            valueEditText.removeTextChangedListener(this)
            valueEditText.setText(item.value.formatToString())
            valueEditText.addTextChangedListener(this)
        }

        override fun afterTextChanged(s: Editable) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                callback?.onCurrencyChanged(list[position], s.toString().toDoubleOrNull())
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // nothing
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            // nothing
        }

    }

    interface Callback {

        fun onFocusChanged(currency: Currency, hasFocus: Boolean)

        fun onCurrencyChanged(currency: Currency, newValue: Double?)

    }

}