package com.example.r12.revoluttest.annotations

import androidx.annotation.LayoutRes


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class ContentView(@LayoutRes val res: Int)