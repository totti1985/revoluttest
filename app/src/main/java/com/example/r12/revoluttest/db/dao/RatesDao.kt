package com.example.r12.revoluttest.db.dao

import androidx.room.*
import com.example.r12.revoluttest.db.entities.RatesRoom

@Dao
interface RatesDao {

    @Query("SELECT * FROM rates WHERE baseCurrency = :baseCurrency LIMIT 1")
    suspend fun getRates(baseCurrency: String): RatesRoom?

    @Transaction
    suspend fun deleteAndInsert(rates: RatesRoom) {
        deleteByBaseCurrency(rates.baseCurrency)
        insertRates(rates)
    }

    @Query("DELETE FROM rates WHERE baseCurrency = :baseCurrency")
    suspend fun deleteByBaseCurrency(baseCurrency: String)

    @Delete()
    suspend fun delete(rates: RatesRoom)

    @Query("DELETE FROM rates")
    fun clearTable()

    @Insert
    suspend fun insertRates(rates: RatesRoom)

    @Query("SELECT * FROM rates ORDER BY savedTime DESC")
    suspend fun getLastSavedRate(): RatesRoom?

}