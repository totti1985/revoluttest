package com.example.r12.revoluttest.backend.api

import com.example.r12.revoluttest.backend.entities.RatesBody
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {

    @GET("/latest")
    fun getRates(@Query("base") base: String): Deferred<RatesBody>

}