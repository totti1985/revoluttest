package com.example.r12.revoluttest.ui.screens.base

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import com.example.r12.revoluttest.annotations.ContentView
import com.omegar.mvp.MvpAppCompatActivity
import kotlin.reflect.full.findAnnotation

@SuppressLint("Registered")
abstract class BaseActivity : MvpAppCompatActivity(), BaseView {

    abstract val presenter: BasePresenter<*>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this::class.findAnnotation<ContentView>()?.let {
            setContentView(it.res)
        }
    }

    fun <T : View> bind(@IdRes res: Int): Lazy<T> {
        return lazy(LazyThreadSafetyMode.NONE) {
            this.findViewById(res) as T
        }
    }

    override fun showToast(@StringRes message: Int) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

}