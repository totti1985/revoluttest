package com.example.r12.revoluttest.di.modules

import com.example.r12.revoluttest.db.RoomDatabase
import com.example.r12.revoluttest.logical.data_sources.rates.BackendRatesDataSource
import com.example.r12.revoluttest.logical.data_sources.rates.RatesDataSource
import com.example.r12.revoluttest.logical.data_sources.rates.RatesDataSourceImpl
import com.example.r12.revoluttest.logical.data_sources.rates.RoomRatesDataSource
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class RatesModule {

    @Provides
    @Singleton
    fun provide(backendRatesDataSource: BackendRatesDataSource, roomRatesDataSource: RoomRatesDataSource): RatesDataSource =
            RatesDataSourceImpl(roomRatesDataSource, backendRatesDataSource)

    @Provides
    @Singleton
    fun provideBackendDataSource(retrofit: Retrofit) = BackendRatesDataSource(retrofit)

    @Provides
    @Singleton
    fun provideRoomDataSource(roomDatabase: RoomDatabase) =
            RoomRatesDataSource(roomDatabase.getRatesDao())

}