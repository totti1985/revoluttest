package com.example.r12.revoluttest

import android.annotation.SuppressLint
import com.example.r12.revoluttest.di.AppComponent
import com.example.r12.revoluttest.di.DaggerAppComponent
import com.example.r12.revoluttest.di.modules.ContextModule
import com.example.r12.revoluttest.di.modules.RetrofitModule

// Only for tests
@SuppressLint("Registered")
class TestRevolutApplication : RevolutApplication() {

    override fun initAppComponent(): AppComponent {
        return DaggerAppComponent.builder()
                .contextModule(ContextModule(this))
                .retrofitModule(RetrofitModule("http://localhost:8080/"))
                .build()
    }

}