package com.example.r12.revoluttest.ui.screens.base.list

import com.example.r12.revoluttest.ui.screens.base.BasePresenter

open class BaseListPresenter<M, T : BaseListView<M>> : BasePresenter<T>()