package com.example.r12.revoluttest.logical.data_sources.rates

import com.example.r12.revoluttest.backend.api.RatesApi
import com.example.r12.revoluttest.backend.entities.RatesBody
import com.example.r12.revoluttest.entities.Rates
import retrofit2.Retrofit

class BackendRatesDataSource(retrofit: Retrofit) : RatesDataSource {

    private val currenciesApi = retrofit.create(RatesApi::class.java)

    override suspend fun requestRates(base: String): RatesBody {
        return currenciesApi.getRates(base).await()
    }

    override suspend fun requestSaveRates(rates: Rates) {
        throw IllegalStateException("BackendRatesDataSource doesn't support save Rates")
    }

    override suspend fun getLastRequestedType(): String? {
        throw IllegalStateException("BackendRatesDataSource doesn't support getLastRequestedType")
    }

}