package com.example.r12.revoluttest.logical.data_sources.rates

import com.example.r12.revoluttest.entities.Rates
import java.util.*
import java.util.Collections.synchronizedMap
import java.util.concurrent.TimeUnit
import kotlin.math.abs

class RatesDataSourceImpl(
        private val roomRatesDataSource: RoomRatesDataSource,
        private val backendRatesDataSource: BackendRatesDataSource
) : RatesDataSource {

    companion object {

        private val EXCEPTION_DELAY_IN_MILLIS = TimeUnit.SECONDS.toMillis(15)

    }

    private val exceptionTimeMap: MutableMap<String, Date?> = synchronizedMap(HashMap<String, Date?>())

    override suspend fun requestSaveRates(rates: Rates) {
        roomRatesDataSource.requestSaveRates(rates)
    }

    override suspend fun requestRates(base: String): Rates {
        try {
            val rates = backendRatesDataSource.requestRates(base)
            roomRatesDataSource.requestSaveRates(rates)
            exceptionTimeMap[base] = null
            return rates
        } catch (exc: Exception) {
            val rates = roomRatesDataSource.requestRates(base)
            rates?.let {
                val lastExceptionTime = exceptionTimeMap[base]
                lastExceptionTime?.let {
                    val diff = abs(Date().time - it.time)
                    if (diff >= EXCEPTION_DELAY_IN_MILLIS) throw exc
                }
                if (lastExceptionTime == null) {
                    exceptionTimeMap[base] = Date()
                }
                return rates
            }
            throw exc
        }
    }

    override suspend fun getLastRequestedType(): String? =
            roomRatesDataSource.getLastRequestedType()

}