package com.example.r12.revoluttest.entities

interface Currency {

    val name: String
    val value: Double?

}

data class EditableCurrency(
        override val name: String,
        override var value: Double?
) : Currency {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EditableCurrency

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    override fun toString(): String {
        return "name $name value $value"
    }
}