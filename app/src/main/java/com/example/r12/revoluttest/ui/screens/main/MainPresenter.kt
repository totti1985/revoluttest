package com.example.r12.revoluttest.ui.screens.main

import com.example.r12.revoluttest.R
import com.example.r12.revoluttest.RevolutApplication
import com.example.r12.revoluttest.entities.Currency
import com.example.r12.revoluttest.entities.EditableCurrency
import com.example.r12.revoluttest.entities.Rates
import com.example.r12.revoluttest.extensions.indexOf
import com.example.r12.revoluttest.extensions.moveTo
import com.example.r12.revoluttest.ui.screens.base.BasePresenter
import com.example.r12.revoluttest.ui.screens.base.list.BaseListPresenter
import com.omegar.mvp.InjectViewState
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.TimeUnit

@InjectViewState
class MainPresenter : BaseListPresenter<Currency, MainView>() {

    companion object {

        private const val INITIAL_TYPE = "EUR"
        private const val INITIAL_SUM = 1.0
        private val REQUEST_DELAY_IN_MILLIS = TimeUnit.SECONDS.toMillis(1)

    }

    private val dataSource = RevolutApplication.getAppComponent().getRatesDataSource()
    private val currenciesList: MutableList<EditableCurrency> = CopyOnWriteArrayList()
    private var initialType: String = INITIAL_TYPE
    private var requestJob: Job? = null
    private var rates: Rates? = null

    init {
        viewState.setWaiting(true)
        launch {
            initialType = dataSource.getLastRequestedType() ?: INITIAL_TYPE
            requestRates()
        }
    }

    fun requestRefreshData() {
        requestRates()
    }

    private fun requestRates() {
        val type = currenciesList.firstOrNull()?.name ?: initialType
        requestJob?.cancel()
        requestJob = launch {
            repeat(Int.MAX_VALUE) {
                dataSource.requestRates(type)?.let {
                    onDataLoaded(it)
                }
                delay(REQUEST_DELAY_IN_MILLIS)
            }
        }
        requestJob?.invokeOnCompletion(this@MainPresenter::invokeOnCompletion)
    }

    private fun invokeOnCompletion(throwable: Throwable?) {
        viewState.setWaiting(false)
        if (throwable == null) {
            // When repeating finished (Int.MAX_VALUE)
            requestRates()
            return
        }
        val message: Int? = when (throwable) {
            is CancellationException -> null // When we cancel job manually
            is UnknownHostException, is ConnectException, is SocketTimeoutException -> R.string.error_internet_conntection
            else -> R.string.error_unknown
        }
        message?.let { viewState.showToast(it) }
    }

    private fun onDataLoaded(rates: Rates) {
        currenciesList.firstOrNull()?.let {
            if (rates.baseCurrency != it.name) {
                requestRates()
                return
            }
        }

        this.rates = rates
        viewState.setWaiting(false)
        if (currenciesList.isEmpty()) {
            currenciesList.add(EditableCurrency(rates.baseCurrency, INITIAL_SUM))
            rates.dependantRatesMap.forEach { coeff ->
                currenciesList.add(EditableCurrency(coeff.key, coeff.value))
            }
            viewState.setList(currenciesList)
            return
        }

        invalidateCurrencies()
    }

    fun requestUpdateCurrency(currency: Currency, newValue: Double?) {
        currenciesList.firstOrNull {
            it.name == currency.name
        }?.let {
            it.value = newValue
            invalidateCurrencies()
        }
    }

    fun requestMoveToTop(currency: Currency) {
        val firstCurrency = currenciesList.firstOrNull()
        if (currency == firstCurrency || currency !is EditableCurrency) return

        val currentIndex = currenciesList.indexOf(currency)
        currenciesList.moveTo(0, currency)
        viewState.notifyItemMoved(currentIndex, 0)
        requestRates()
    }

    private fun invalidateCurrencies() {
        if (rates == null) return

        val requestedType = rates!!.baseCurrency
        val coeffMap = rates!!.dependantRatesMap

        val mainCurrency = currenciesList.firstOrNull() ?: return
        val mainValue = mainCurrency.value ?: INITIAL_SUM

        if (requestedType == mainCurrency.name) {
            invalidateCurrencies(mainValue)
        } else {
            // If we change main type, but doesn't receive rates
            // We calculate it manually
            val requestedTypeIndex = currenciesList.indexOf(requestedType)
            if (requestedTypeIndex == -1) return
            val requestedCurrency = currenciesList[requestedTypeIndex]

            val coeff = coeffMap[mainCurrency.name] ?: return
            val newMainValue = mainValue / coeff
            requestedCurrency.value = newMainValue

            invalidateCurrencies(newMainValue)
        }

        if (currenciesList.size > 1) viewState.notifyItemRangeChanged(1, currenciesList.size - 1)
    }

    private fun invalidateCurrencies(mainValue: Double) {
        if (rates == null || currenciesList.size <= 1) return
        val coeffMap = rates?.dependantRatesMap!!

        for (i in 1 until currenciesList.size) {
            val currency = currenciesList[i]
            coeffMap[currency.name]?.let {
                currency.value = it * mainValue
            }
        }
    }

}