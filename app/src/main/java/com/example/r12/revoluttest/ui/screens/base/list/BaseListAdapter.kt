package com.example.r12.revoluttest.ui.screens.base.list

import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

abstract class BaseListAdapter<T> : RecyclerView.Adapter<BaseListAdapter.ViewHolder<T>>() {

    var list: List<T> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder<T>, position: Int) {
        holder.bind(list[position])
    }

    abstract class ViewHolder<T>(
            @LayoutRes layoutRes: Int,
            parent: ViewGroup
    ) : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)) {

        abstract fun bind(item: T)

    }

}