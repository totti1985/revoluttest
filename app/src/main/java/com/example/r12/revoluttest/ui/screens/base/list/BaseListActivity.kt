package com.example.r12.revoluttest.ui.screens.base.list

import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.r12.revoluttest.ui.screens.base.BaseActivity

abstract class BaseListActivity<T> : BaseActivity(), BaseListView<T> {

    abstract override val presenter: BaseListPresenter<T, *>
    protected abstract val adapter: BaseListAdapter<T>
    protected abstract val recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        recyclerView.adapter = adapter
    }

    override fun setList(list: List<T>) {
        adapter.list = list
    }

}