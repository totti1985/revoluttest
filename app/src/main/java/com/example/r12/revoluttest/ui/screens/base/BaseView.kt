package com.example.r12.revoluttest.ui.screens.base

import androidx.annotation.StringRes
import com.omegar.mvp.MvpView
import com.omegar.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.omegar.mvp.viewstate.strategy.StateStrategyType

interface BaseView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showToast(@StringRes message: Int)

}