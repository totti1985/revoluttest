package com.example.r12.revoluttest.ui.screens.base

import com.omegar.mvp.MvpPresenter
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


open class BasePresenter<T : BaseView> : MvpPresenter<T>(), CoroutineScope {

    private val job = SupervisorJob()

    private val handler = CoroutineExceptionHandler { _, exception ->
        handleError(exception)
    }

    override val coroutineContext: CoroutineContext = Dispatchers.Main + job + handler

    protected open fun handleError(exc: Throwable) {
        exc.printStackTrace()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

}