package com.example.r12.revoluttest.ui.screens.base.list

import com.example.r12.revoluttest.ui.screens.base.BaseView
import com.omegar.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.omegar.mvp.viewstate.strategy.StateStrategyType

interface BaseListView<T> : BaseView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setList(list: List<T>)

}