package com.example.r12.revoluttest.ui.screens.main

import com.example.r12.revoluttest.entities.Currency
import com.example.r12.revoluttest.ui.screens.base.BaseView
import com.example.r12.revoluttest.ui.screens.base.list.BaseListView
import com.omegar.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.omegar.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.omegar.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface MainView : BaseListView<Currency> {

    fun setWaiting(waiting: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun notifyItemMoved(from: Int, to: Int)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun notifyItemRangeChanged(from: Int, to: Int)

}