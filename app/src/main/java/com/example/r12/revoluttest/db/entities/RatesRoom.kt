package com.example.r12.revoluttest.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.r12.revoluttest.db.converters.DateConverter
import com.example.r12.revoluttest.db.converters.MapConverter
import com.example.r12.revoluttest.entities.Rates
import java.util.*

@Entity(tableName = "rates")
@TypeConverters(MapConverter::class, DateConverter::class)
class RatesRoom(
        @ColumnInfo
        @PrimaryKey
        override val baseCurrency: String,
        @ColumnInfo
        override val dependantRatesMap: Map<String, Double>,
        @ColumnInfo
        val savedTime: Date = Date()
) : Rates {

        constructor(rates: Rates) : this(rates.baseCurrency, rates.dependantRatesMap)

}