package com.example.r12.revoluttest.entities

interface Rates {

    val baseCurrency: String
    val dependantRatesMap: Map<String, Double>

}