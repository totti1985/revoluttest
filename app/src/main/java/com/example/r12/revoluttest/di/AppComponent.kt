package com.example.r12.revoluttest.di

import com.example.r12.revoluttest.di.modules.ContextModule
import com.example.r12.revoluttest.di.modules.RatesModule
import com.example.r12.revoluttest.di.modules.RetrofitModule
import com.example.r12.revoluttest.di.modules.RoomDatabaseModule
import com.example.r12.revoluttest.logical.data_sources.rates.RatesDataSource
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ContextModule::class,
    RetrofitModule::class,
    RatesModule::class,
    RoomDatabaseModule::class
])
interface AppComponent {

    fun getRatesDataSource(): RatesDataSource

}