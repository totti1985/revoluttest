package com.example.r12.revoluttest.ui.screens.main

import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.r12.revoluttest.R
import com.example.r12.revoluttest.annotations.ContentView
import com.example.r12.revoluttest.entities.Currency
import com.example.r12.revoluttest.ui.screens.base.list.BaseListActivity
import com.omegar.mvp.presenter.InjectPresenter

@ContentView(R.layout.activity_main)
class MainActivity : BaseListActivity<Currency>(), MainView, CurrencyAdapter.Callback {

    @InjectPresenter
    override lateinit var presenter: MainPresenter

    override val recyclerView: RecyclerView by bind(R.id.recyclerview)
    private val swipeRefreshLayout: SwipeRefreshLayout by bind(R.id.swiperefreshlayout)
    override val adapter = CurrencyAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        swipeRefreshLayout.setOnRefreshListener(presenter::requestRefreshData)
        recyclerView.isFocusableInTouchMode = false // Fix bug - changing editText input type on scroll
    }

    override fun setWaiting(waiting: Boolean) {
        swipeRefreshLayout.isRefreshing = waiting
    }

    override fun onCurrencyChanged(currency: Currency, newValue: Double?) {
        presenter.requestUpdateCurrency(currency, newValue)
    }

    override fun onFocusChanged(currency: Currency, hasFocus: Boolean) {
        if (!hasFocus) return
        if (isRecyclerNotReady()) {
            recyclerView.post { onFocusChanged(currency, hasFocus) }
        } else {
            presenter.requestMoveToTop(currency)
        }
    }

    override fun notifyItemMoved(from: Int, to: Int) {
        if (isRecyclerNotReady()) {
            recyclerView.post { notifyItemMoved(from, to) }
        } else {
            adapter.notifyItemMoved(from, to)
        }
    }

    override fun notifyItemRangeChanged(from: Int, to: Int) {
        if (isRecyclerNotReady()) {
            recyclerView.post { notifyItemRangeChanged(from, to) }
        } else {
            adapter.notifyItemRangeChanged(from, to, false)
        }
    }

    private fun isRecyclerNotReady(): Boolean {
        return recyclerView.isAnimating || recyclerView.isComputingLayout
    }
}