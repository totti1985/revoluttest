package com.example.r12.revoluttest.extensions

import com.example.r12.revoluttest.entities.Currency

fun <E> MutableList<E>.moveTo(position: Int, e: E) {
    if (position >= this.size) return

    val index = this.indexOf(e)
    if (index == -1 || position >= index) return

    removeAt(index)
    add(position, e)
}

fun List<Currency>.indexOf(type: String): Int {
    for (i in 0 until size) {
        if (get(i).name == type) return i
    }
    return -1
}