package com.example.r12.revoluttest.base

import android.content.Context
import androidx.annotation.CallSuper
import com.example.r12.revoluttest.backend.entities.RatesBody
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.mockito.Mock
import org.mockito.MockitoAnnotations

open class BaseTest {

    @Mock
    lateinit var context: Context
    protected val webServer: MockWebServer = MockWebServer()
    protected val rates = RatesBody("EUR", mapOf(Pair("RU", 74.0), Pair("USD", 1.1)))

    @Before
    @CallSuper
    open fun setup() {
        MockitoAnnotations.initMocks(this)
        webServer.start(8080)
    }

    @After
    fun finish() {
        webServer.shutdown()
    }

}