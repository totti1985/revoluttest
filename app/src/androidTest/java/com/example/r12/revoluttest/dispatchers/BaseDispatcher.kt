package com.example.r12.revoluttest.dispatchers

import com.google.gson.Gson
import okhttp3.mockwebserver.Dispatcher

abstract class BaseDispatcher : Dispatcher() {

    companion object {

        @JvmStatic
        protected val json = Gson()

    }

}