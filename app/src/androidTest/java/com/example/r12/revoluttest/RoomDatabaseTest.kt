package com.example.r12.revoluttest

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.r12.revoluttest.db.RoomDatabase
import com.example.r12.revoluttest.db.dao.RatesDao
import com.example.r12.revoluttest.db.entities.RatesRoom
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertNull
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RoomDatabaseTest {

    private lateinit var db: RoomDatabase
    private lateinit var ratesDao: RatesDao
    private val rates = RatesRoom("EUR", mapOf())

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, RoomDatabase::class.java).build()
        ratesDao = db.getRatesDao()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun testInsert() {
        runBlocking {
            ratesDao.insertRates(rates)
            assertNotNull(ratesDao.getRates(rates.baseCurrency))
        }
    }

    @Test
    fun testDeleteAndInsert() {
        runBlocking {
            ratesDao.deleteAndInsert(rates)
            assertNotNull(ratesDao.getRates(rates.baseCurrency))
        }
    }

    @Test
    fun testDelete() {
        runBlocking {
            ratesDao.insertRates(rates)
            ratesDao.delete(rates)
            assertNull(ratesDao.getRates(rates.baseCurrency))
        }
    }

    @Test
    fun testDeleteByBaseCurrency() {
        runBlocking {
            ratesDao.insertRates(rates)
            ratesDao.deleteByBaseCurrency(rates.baseCurrency)
            assertNull(ratesDao.getRates(rates.baseCurrency))
        }
    }

    @Test
    fun testEmptyBase() {
        runBlocking {
            ratesDao.clearTable()
            assertNull(ratesDao.getRates(rates.baseCurrency))
        }
    }

}