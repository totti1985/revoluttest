package com.example.r12.revoluttest

import com.example.r12.revoluttest.base.BaseTest
import com.example.r12.revoluttest.dispatchers.RatesDispatcher
import com.example.r12.revoluttest.entities.Currency
import com.example.r12.revoluttest.ui.screens.main.MainPresenter
import com.example.r12.revoluttest.ui.screens.main.`MainView$$State`
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatcher
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.timeout
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.TimeUnit

@RunWith(MockitoJUnitRunner::class)
class MainPresenterTest : BaseTest() {

    @Mock
    private lateinit var view: `MainView$$State`

    @Test
    fun testSetWaiting() {
        webServer.setDispatcher(RatesDispatcher.SuccessDispatcher(rates))
        val presenter = MainPresenter()
        presenter.setViewState(view)
        presenter.requestRefreshData()
        Mockito.verify(view, timeout(2000)).setWaiting(false)
    }

    @Test
    fun testSuccess() {
        webServer.setDispatcher(RatesDispatcher.SuccessDispatcher(rates))
        val presenter = MainPresenter()
        presenter.setViewState(view)
        presenter.requestRefreshData()
        Mockito.verify(view, timeout(2000))
                .setList(ArgumentMatchers.argThat(SizeMatcher()))
    }

    @Test
    fun testFailed() {
        webServer.setDispatcher(RatesDispatcher.FailDispatcher())
        val presenter = MainPresenter()
        presenter.setViewState(view)
        presenter.requestRefreshData()
        Mockito.verify(view, timeout(TimeUnit.MINUTES.toMillis(15))) // Delay from RatesDataSource
                .showToast(ArgumentMatchers.anyInt())
    }

    private inner class SizeMatcher : ArgumentMatcher<List<Currency>> {

        override fun matches(argument: List<Currency>): Boolean =
                argument.size == rates.dependantRatesMap.size + 1 // + 1 for EUR

    }

}