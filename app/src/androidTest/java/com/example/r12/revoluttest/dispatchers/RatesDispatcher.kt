package com.example.r12.revoluttest.dispatchers

import com.example.r12.revoluttest.backend.entities.RatesBody
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest

class RatesDispatcher {

    class SuccessDispatcher(private val body: RatesBody) : BaseDispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            val path = request.path
            if (path.contains("latest", true)) {
                return MockResponse().setResponseCode(200)
                        .setBody(json.toJson(body))
            }
            return MockResponse().setResponseCode(404)
        }
    }

    class FailDispatcher : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return MockResponse().setResponseCode(400)
        }
    }

}