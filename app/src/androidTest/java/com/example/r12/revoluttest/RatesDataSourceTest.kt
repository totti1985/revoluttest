package com.example.r12.revoluttest

import com.example.r12.revoluttest.base.BaseTest
import junit.framework.Assert.assertNotNull
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RatesDataSourceTest : BaseTest() {

    private val dataSource = RevolutApplication.getAppComponent().getRatesDataSource()

    @Test
    fun testLastRequestedType() {
        runBlocking {
            dataSource.requestSaveRates(rates)
            assert(rates.baseCurrency == dataSource.getLastRequestedType())
        }
    }

    @Test
    fun testSuccess() {
        runBlocking {
            dataSource.requestSaveRates(rates)
            assertNotNull(dataSource.requestRates(rates.baseCurrency))
        }
    }

}